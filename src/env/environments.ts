export const environments = {
  firebase: {
    firebaseConfigProd: {},
    firebaseConfigDev: {
      apiKey: 'AIzaSyDIbm3AHz0ykjFD-02qPFoKRnBlO77QIMc',
      authDomain: 'bils-dev.firebaseapp.com',
      databaseURL: 'https://bils-dev.firebaseio.com',
      projectId: 'bils-dev',
      storageBucket: 'bils-dev.appspot.com',
      messagingSenderId: '19879989168',
      appId: '1:19879989168:web:8098f548282db4887e5075',
    },
    firebaseConfigQA: {},
    collections: {
      users: 'users',
      roles: 'roles',
    },
  },
  routes: {
    dotenv: '/../../.env',
  },
};
