export const CONSTANS = {
  environments: {
    PORT: 'PORT',
    FIREBASE: 'FIREBASE',
    NODE_ENV: 'NODE_ENV',
    parameters: {
      DEV: 'DEV',
      PROD: 'PROD',
      QA: 'QA',
      USER_ROLE: 'h2JPuAEg9JNUSxZCsgk6',
      auth: {
        exist: 'auth/email-already-in-use',
        invalid: 'auth/invalid-email',
        notAllowed: 'auth/operation-not-allowed',
        weakPass: 'auth/weak-password',
        network: 'auth/network-request-failed',
      },
    },
  },
};
