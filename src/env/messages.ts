export const MESSAGES = {
  auth: {
    created: 'usuario creado con éxito',
    incompleteError: 'debe enviar todos los campos requeridos',
    firestoreError: 'error al guardar usuario en firestore',
    authError: 'error al registrar usuario',
    errors: {
      exist: 'correo ya esta registrado',
      invalid: 'correo no válido',
      notAllowed: 'sin servicio de autenticación por los momentos',
      weakPass: 'contraseña demasiado debíl',
      network: 'servicio sin conexión a internet',
    },
  },
};
