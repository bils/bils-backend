import { CONSTANS } from '../env/constans';
import { fullActualDate } from './date.util';
import { AppModule } from '../app.module';

const PROD = CONSTANS.environments.parameters.PROD;
const DEV = CONSTANS.environments.parameters.DEV;
const QA = CONSTANS.environments.parameters.QA;

/**
 * @param from desde donde se llama el log
 * @param message mensaje que se mostara
 */
export function log(from: string, message: string) {
  const env = AppModule.env;
  const date = fullActualDate();

  switch (env) {
    case DEV:
      console.log(`LOG -> ${date} [${DEV}] [${from}] ${message}`);
      break;

    case QA:
      console.log(`LOG -> ${date} [${QA}] [${from}] ${message}`);
      break;

    case PROD:
      console.log(`LOG -> ${date} [${PROD}] [${from}] ${message}`);
      break;
  }
}

/**
 * @param from desde donde se llama el log
 * @param message mensaje que se mostara
 * @param trace objeto del error
 */
export function error(from: string, message: string, trace?: any) {
  const env = AppModule.env;
  const date = fullActualDate();
  const json = JSON.stringify(trace);

  switch (env) {
    case DEV:
      console.error(`ERROR -> ${date} [${DEV}] [${from}] ${message}`, json);
      break;

    case QA:
      console.error(`ERROR -> ${date} [${QA}] [${from}] ${message}`, json);
      break;

    case PROD:
      console.error(`ERROR -> ${date} [${PROD}] [${from}] ${message}`, json);
      break;
  }
}

/**
 * @param from desde donde se llama el log
 * @param message mensaje que se mostara
 */
export function warn(from: string, message: string) {
  const env = AppModule.env;
  const date = fullActualDate();

  switch (env) {
    case DEV:
      console.warn(`WARN -> ${date} [${DEV}] [${from}] ${message}`);
      break;

    case QA:
      console.warn(`WARN -> ${date} [${QA}] [${from}] ${message}`);
      break;

    case PROD:
      console.warn(`WARN -> ${date} [${PROD}] [${from}] ${message}`);
      break;
  }
}
