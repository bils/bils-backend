/**
 * Retorna la fecha actual con el formato chileno incluyendo la hora
 */
export function fullActualDate(): string {
  const date = new Date();
  const day = date.getDate();
  const month = date.getMonth().valueOf() + 1;
  const year = date.getFullYear();
  const hour = date.getHours();
  const min = date.getMinutes();

  let dd = null;
  let mm = null;
  let hh = null;
  let m = null;

  if (day < 10) {
    dd = '0' + day;
  } else {
    dd = day;
  }

  if (month < 10) {
    mm = '0' + month;
  } else {
    mm = month;
  }

  if (hour < 10) {
    hh = '0' + hour;
  } else {
    hh = hour;
  }

  if (min < 10) {
    m = '0' + min;
  } else {
    m = min;
  }

  return `${dd}/${mm}/${year} ${hh}:${m}`;
}

/**
 * Retorna la fecha actual con el formato chileno solo la fecha
 */
export function onlyActualDate(): string {
  const date = new Date();
  const day = date.getDate();
  const month = date.getMonth().valueOf() + 1;
  const year = date.getFullYear();

  let dd = null;
  let mm = null;

  if (day < 10) {
    dd = '0' + day;
  } else {
    dd = day;
  }

  if (month < 10) {
    mm = '0' + month;
  } else {
    mm = month;
  }

  return `${dd}/${mm}/${year}`;
}
