import { Module } from '@nestjs/common';

// Modules
import { ConfigModule } from './config/config.module';
import { RoutesModule } from './routes/routes.module';

// Controllers
import { AppController } from './app.controller';

// Services
import { AppService } from './app.service';
import { ConfigService } from './config/config.service';

// Constans
import { CONSTANS } from './env/constans';

@Module({
  imports: [ConfigModule, RoutesModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  public static port: number | string;
  public static env: string;

  constructor(readonly _config: ConfigService) {
    AppModule.port = _config.get(CONSTANS.environments.PORT);
    AppModule.env = _config.get(CONSTANS.environments.NODE_ENV);
  }
}
