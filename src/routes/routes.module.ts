import { Module } from '@nestjs/common';

// Controllers
import { AuthController } from './auth/auth.controller';

// Services
import { AuthService } from './auth/auth.service';

@Module({
  controllers: [AuthController],
  providers: [AuthService],
})
export class RoutesModule {}
