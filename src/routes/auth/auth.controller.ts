import { Controller, Post, Body, Res, HttpStatus } from '@nestjs/common';
import { Response } from 'express';

// Services
import { AuthService } from './auth.service';

// Interfaces
import { Register, Login, ResponseAuth } from './auth.interface';

@Controller('auth')
export class AuthController {
  constructor(private readonly _authService: AuthService) {}

  /**
   * @param res response de express
   * @param user body de la peticion
   */
  @Post('login')
  public login(@Res() res: Response, @Body() user: Login) {}

  /**
   * @param res response de express
   * @param user body de la peticion
   */
  @Post('register')
  public register(@Res() res: Response, @Body() user: Register): void {
    this._authService
      .register(user)
      .then((data: ResponseAuth) => {
        res.status(HttpStatus.CREATED).json(data);
      })
      .catch((err: ResponseAuth) => {
        res.status(err.status).json(err);
      });
  }
}
