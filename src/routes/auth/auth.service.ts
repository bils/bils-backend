import { Injectable } from '@nestjs/common';

// Firebase
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

// Interfaces
import { Register, ResponseAuth } from './auth.interface';

// Utils
import * as logger from '../../utils/logger.util';

// Environmens
import { environments } from '../../env/environments';
import { CONSTANS } from '../../env/constans';
import { MESSAGES } from '../../env/messages';

const NAME = 'AuthService';

@Injectable()
export class AuthService {
  private auth: firebase.auth.Auth;
  private firestore: firebase.firestore.Firestore;

  constructor() {
    this.auth = firebase.auth();
    this.firestore = firebase.firestore();
  }

  public login(): Promise<ResponseAuth> {
    return new Promise((resolve, reject) => {
      resolve();
    });
  }

  /**
   * @param authRegister usuario que se guarda en la bd
   *
   * Registra un usuario dentro de la aplicacion
   */
  public register(authRegister: Register): Promise<ResponseAuth> {
    return new Promise((resolve, reject) => {
      logger.log(NAME, 'INIT REGISTER USER: ' + authRegister.run);

      if (!this.validateBodyAuth(authRegister)) {
        logger.error(NAME, 'BODY IMCOMPLETE');
        reject({
          ok: false,
          status: 400,
          message: MESSAGES.auth.incompleteError,
          error: MESSAGES.auth.incompleteError,
        });
      }

      authRegister = this.setDefaultRegister(authRegister);
      const password = authRegister.password;
      delete authRegister.password;

      this.auth
        .createUserWithEmailAndPassword(authRegister.email, password)
        .then((user: firebase.auth.UserCredential) => {
          logger.log(
            NAME,
            'END USER REGISTER IN AUTH WAITING SAVE FIRESTORE...',
          );
          authRegister.uid = user.user.uid;
          this.saveUser(authRegister)
            .then(userSave => {
              resolve({
                ok: true,
                status: 201,
                message: MESSAGES.auth.created,
                user: userSave,
              });
            })
            .catch(err => {
              reject({
                ok: false,
                status: 500,
                message: MESSAGES.auth.firestoreError,
                error: err,
              });
            });
        })
        .catch(err => {
          logger.error(NAME, 'ERROR REGISTER USER: ' + authRegister.run, err);
          reject({
            ok: false,
            status: 500,
            message: this.getMessageErrorAuth(err.code),
            error: err,
          });
        });
    });
  }

  /**
   * @param authRegister usuario que se guarda en la bd
   *
   * Guarda un usuario en la base de datos
   */
  private saveUser(authRegister: Register): Promise<Register | any> {
    return new Promise((resolve, reject) => {
      logger.log(NAME, 'INIT SAVE USER FIRESTORE');

      this.firestore
        .collection(environments.firebase.collections.users)
        .doc(authRegister.uid)
        .set(authRegister)
        .then(() => {
          logger.log(NAME, 'END SAVE USER FIRESTORE');
          resolve(authRegister);
        })
        .catch(err => {
          logger.error(NAME, 'ERROR SAVE USER FIRESTORE', err);
          reject(err);
        });
    });
  }

  /**
   * @param authRegister usuario que se guarda en la bd
   *
   * Setea los valores por defecto
   */
  private setDefaultRegister(authRegister: Register): Register {
    authRegister.active = true;
    authRegister.role = authRegister.role
      ? authRegister.role
      : CONSTANS.environments.parameters.USER_ROLE;
    return authRegister;
  }

  /**
   * @param error mensaje de error de firebase
   *
   * retorna un error personalizado
   */
  private getMessageErrorAuth(error: string): string {
    switch (error) {
      case CONSTANS.environments.parameters.auth.exist:
        return MESSAGES.auth.errors.exist;
      case CONSTANS.environments.parameters.auth.invalid:
        return MESSAGES.auth.errors.invalid;
      case CONSTANS.environments.parameters.auth.notAllowed:
        return MESSAGES.auth.errors.notAllowed;
      case CONSTANS.environments.parameters.auth.weakPass:
        return MESSAGES.auth.errors.weakPass;
      case CONSTANS.environments.parameters.auth.network:
        return MESSAGES.auth.errors.network;
    }
  }

  /**
   * @param authRegister usuario que se guarda en la bd
   *
   * Valida que todos parametros del body existan
   */
  private validateBodyAuth(authRegister: Register): boolean {
    return authRegister.name &&
      authRegister.lastname &&
      authRegister.run &&
      authRegister.password &&
      authRegister.phoneNumber &&
      authRegister.address &&
      authRegister.commune &&
      authRegister.age
      ? true
      : false;
  }
}
