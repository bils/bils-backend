export interface Login {
  email: string;
  password: string;
}

export interface Register {
  name: string;
  lastname: string;
  run: string;
  email: string;
  password: string;
  phoneNumber: string;
  address: string;
  commune: string;
  age: number;
  active: boolean;
  role: any;
  uid?: string;
}

export interface ResponseAuth {
  ok: boolean;
  status: number;
  message: string;
  error?: any;
  user?: Register;
}
