import * as fs from 'fs';
import * as firebase from 'firebase/app';
import { parse } from 'dotenv';

// Config
import { Config } from './config.interface';

// Environment
import { CONSTANS } from '../env/constans';
import { environments } from '../env/environments';

// Utils
import { fullActualDate } from '../utils/date.util';

const envFilePath = __dirname + environments.routes.dotenv;

export class ConfigService {
  private envConfig: Config;

  constructor() {
    this.initConfig();
  }

  /**
   * Inicializa las distintas configuraciones del proyecto
   */
  private initConfig() {
    this.getEnv()
      .then((res: string) => {
        console.log(res);
        this.initFirebase();
      })
      .catch((err: string) => {
        console.log(err);
        process.exit(0);
      });
  }

  /**
   * Verifica si estamos en dev o prod
   */
  private getEnv(): Promise<string> {
    return new Promise((resolve, reject) => {
      const isDevelopmentEnv =
        process.env.NODE_ENV !== CONSTANS.environments.parameters.PROD &&
        process.env.NODE_ENV !== CONSTANS.environments.parameters.QA;

      if (isDevelopmentEnv) {
        // Desarrollo
        if (!this.existFilePath()) {
          reject(
            `ERROR -> [${fullActualDate()}] [DEV] .env file does not exist`,
          );
        }

        this.envConfig = parse(this.getFilePath());
        resolve(`LOG -> [${fullActualDate()}] [DEV] .env successful`);
      } else {
        // Produccion
        this.envConfig = {
          PORT: process.env.PORT,
          FIREBASE: process.env.FIREBASE,
          NODE_ENV: process.env.NODE_ENV,
        };
        resolve(`LOG -> [${fullActualDate()}] [PROD] .env successful`);
      }
    });
  }

  /**
   * Devuelve si existe o no existe el archivo
   */
  private existFilePath(): boolean {
    return fs.existsSync(envFilePath);
  }

  /**
   * Lee y retorna el archivo de environment
   */
  private getFilePath(): Buffer {
    return fs.readFileSync(envFilePath);
  }

  private initFirebase() {
    switch (this.get(CONSTANS.environments.NODE_ENV)) {
      case CONSTANS.environments.parameters.DEV:
        firebase.initializeApp(environments.firebase.firebaseConfigDev);
        console.log(`LOG -> [${fullActualDate()}] [DEV] Firebase init`);
        break;

      case CONSTANS.environments.parameters.QA:
        firebase.initializeApp(environments.firebase.firebaseConfigQA);
        console.log(`LOG -> [${fullActualDate()}] [QA] Firebase init`);
        break;

      case CONSTANS.environments.parameters.PROD:
        firebase.initializeApp(environments.firebase.firebaseConfigProd);
        console.log(`LOG -> [${fullActualDate()}] [PROD] Firebase init`);
        break;
    }
  }

  /**
   * @param key el parametro que se desea retornar
   *
   * Retorna el environment que deseemos.
   */
  public get(key: string): string {
    return this.envConfig[key];
  }
}
