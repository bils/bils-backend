import { NestFactory } from '@nestjs/core';
import { NestApplicationOptions } from '@nestjs/common';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import * as helmet from 'helmet';

const options: NestApplicationOptions = {
  logger: ['error', 'warn', 'log'],
};

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(
    AppModule,
    options,
  );
  app.use(helmet());
  app.setGlobalPrefix('/api/v1');
  await app.listen(AppModule.port);
}
bootstrap();
